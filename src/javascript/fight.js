export function fight(firstFighter, secondFighter) {
  let healthFirstFighter = firstFighter.health;
  let healthSecondFighter = secondFighter.health;
  do {
    healthFirstFighter = healthFirstFighter - getDamage(secondFighter, firstFighter);
    healthSecondFighter = healthSecondFighter - getDamage(firstFighter, secondFighter);
  } while ((healthFirstFighter > 0) && (healthSecondFighter > 0));
  const winner = (healthFirstFighter <= 0) ? secondFighter : firstFighter;
  return winner;
}

export function getDamage(attacker, enemy) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(enemy);
  const damage = Math.max(hitPower - blockPower, 0);
  return damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const { attack } = fighter;
  const hitPower = attack * criticalHitChance;
  return hitPower;

}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  const { defense } = fighter;
  const blockPower = defense * dodgeChance;
  return blockPower;
}
