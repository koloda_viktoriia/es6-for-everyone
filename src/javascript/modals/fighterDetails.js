import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createAttribute(name, 'Name', 'fighter-name');
  const attackElement = createAttribute(attack, 'Attack', 'fighter-attack');
  const defenseElement = createAttribute(defense, 'Defense', 'fighter-defense');
  const healthElement = createAttribute(health, 'Health', 'fighter-health');
  const imageElement = createImage(source);
  fighterDetails.append(nameElement, attackElement, defenseElement, healthElement, imageElement);
  return fighterDetails;
}

export function createAttribute(element, title, classElement) {
  const nameElement = createElement({ tagName: 'span', className: classElement });
  nameElement.innerText = title + ": " + element + '\n';
  return nameElement;
}

export function createImage(source) {
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  return imgElement;
}