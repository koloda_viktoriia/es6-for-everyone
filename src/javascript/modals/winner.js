import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { createImage, createAttribute } from './fighterDetails';

export function showWinnerModal(fighter) {

  const title = 'Winner!';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement });

}
function createWinnerDetails(fighter) {
  const { name, source } = fighter;
  const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createAttribute(name, 'Name', 'fighter-name');
  const imageElement = createImage(source);
  winnerDetails.append(nameElement, imageElement);
  return winnerDetails;
}